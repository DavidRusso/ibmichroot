#------IMPORTANT WARNING -------
This yum part of project is under construction. APIs are changing daily, therefore, you should not use for ANY production purpose. 
When this warning disappears, APIs will be considered stable.


#IBM i yum
We are trying to get yum working on IBM i to replace poor man's yum(ish) pkg_setup.sh and *.lst(s).

#WARNING
Only new chroots with this new code have been tried. 
We have no idea what will happen if you attempt to update an old GCC chroot environment.
 

#yum setup
chroot_setup.sh -- set up chroot
```
$ cd /QOpenSys/ibmichroot
$ chroot_setup.sh -help
$ chroot_setup.sh chroot_minimal.lst /QOpenSys/root_path [dynamic global variables]
$ cp -R /QOpenSys/ibmichroot /QOpenSys/root_path/QOpenSys/.
```
Enter chroot
```
$ chroot /QOpenSys/root_path /usr/bin/sh
```
Inside your new chroot:
```
> export PATH=/opt/freeware/bin:/usr/bin:.
> export LIBPATH=/opt/freeware/lib:/usr/lib:.
> cd /QOpenSys/ibmichroot/yum
> ./setup_rpm4_and_yum.sh
```

If it worked (cross fingers) ...
```
$ rpm --version
RPM version 4.9.1.3
$ yum --version
3.4.3
  Installed: yum-3.4.3-3.noarch at 2016-09-08 21:29
  Built    : None at 2016-08-18 11:06
  Committed: Sangamesh Mallayya <sangamesh.swamy@in.ibm.com> at 2016-08-19
$ yum list
AIX_Toolbox                                                                                                                      | 2.9 kB  00:00:00     
AIX_Toolbox/primary_db                                                                                                           | 185 kB  00:00:00     
AIX_Toolbox_noarch                                                                                                               | 2.9 kB  00:00:00     
AIX_Toolbox_noarch/primary_db                                                                                                    |  15 kB  00:00:00     
Installed Packages
curl.ppc                                                                     7.44.0-1                                                 installed         
db.ppc                                                                       4.8.24-3                                                 installed         
gdbm.ppc                                                                     1.8.3-5                                                  installed         
gettext.ppc                                                                  0.10.40-8                                                installed         
glib2.ppc                                                                    2.14.6-2                                                 installed         
libiconv.ppc                                                                 1.14-2                                                   installed         
libxml2.ppc                                                                  2.9.4-1                                                  installed         
pysqlite.ppc                                                                 1.1.7-1                                                  installed         
python.ppc                                                                   2.7.10-1                                                 installed         
python-devel.ppc                                                             2.7.10-1                                                 installed         
python-iniparse.noarch                                                       0.4-1                                                    installed         
python-pycurl.ppc                                                            7.19.3-1                                                 installed         
python-tools.ppc                                                             2.7.10-1                                                 installed         
python-urlgrabber.noarch                                                     3.10.1-1                                                 installed         
readline.ppc                                                                 6.1-2                                                    installed         
sqlite.ppc                                                                   3.7.15.2-2                                               installed         
wget.ppc                                                                     1.9.1-1                                                  installed         
xz-libs.ppc                                                                  5.0.7-1                                                  installed         
yum.noarch                                                                   3.4.3-3                                                  installed         
yum-metadata-parser.ppc                                                      1.1.4-1                                                  installed         
Available Packages
autoconf.noarch                                                              2.69-1                                                   AIX_Toolbox_noarch
autoconf213.noarch                                                           2.13-1                                                   AIX_Toolbox_noarch
automake.noarch                                                              1.15-1                                                   AIX_Toolbox_noarch
createrepo.noarch                                                            0.10.3-2                                                 AIX_Toolbox_noarch
docbookx.noarch                                                              4.1.2-2                                                  AIX_Toolbox_noarch
help2man.noarch                                                              1.29-1                                                   AIX_Toolbox_noarch
openCIMOM.noarch                                                             2:0.8-1                                                  AIX_Toolbox_noarch
python-argparse.noarch                                                       1.2.1-1                                                  AIX_Toolbox_noarch
python-boto.noarch                                                           2.34.0-1                                                 AIX_Toolbox_noarch
python-configobj.noarch                                                      5.0.5-1                                                  AIX_Toolbox_noarch
python-jsonpatch.noarch                                                      1.8-1                                                    AIX_Toolbox_noarch
python-jsonpointer.noarch                                                    1.0-1                                                    AIX_Toolbox_noarch
python-oauth.noarch                                                          1.0.1-1                                                  AIX_Toolbox_noarch
python-prettytable.noarch                                                    0.7.2-1                                                  AIX_Toolbox_noarch
python-requests.noarch                                                       2.4.3-1                                                  AIX_Toolbox_noarch
python-setuptools.noarch                                                     0.9.8-2                                                  AIX_Toolbox_noarch
python-setuptools-devel.noarch                                               0.9.8-2                                                  AIX_Toolbox_noarch
python-six.noarch                                                            1.3.0-1                                                  AIX_Toolbox_noarch
smake.noarch                                                                 1:1.3.2-1                                                AIX_Toolbox_noarch
urw-fonts.noarch                                                             2.0-1                                                    AIX_Toolbox_noarch
$ 
```


#License
MIT.  View [`LICENSE`](https://bitbucket.org/litmis/ibmichroot/src) file.

#note to myself
```
scp -r * adc@ut28p63:/QOpenSys/yum2/QOpenSys/QIBM/ProdData/OPS/GCC/yum
```
