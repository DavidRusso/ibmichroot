#!/bin/sh
#
# global
#

if [ -d /QOpenSys/usr/bin ]
then
  system_OS400=1
  PATH=/QOpenSys/usr/bin:/QOpenSys/usr/sbin:/opt/freeware/bin
  LIBPATH=/QOpenSys/usr/lib:/opt/freeware/lib
  export PATH
  export LIBPATH
  echo "**********************"
  echo "Live IBM i session (changes made)."
  echo "**********************"
  echo "PATH=$PATH"
  echo "LIBPATH=$LIBPATH"
else
  system_OS400=0
  echo "**********************"
  echo "Not IBM i, downloads only, other no action taken."
  echo "**********************"
fi

RPM_RTE="rpm.rte.4.9.1.3"
RPM_WGET="wget-1.9.1-1.aix5.1.ppc.rpm"
YUM_TAR="yum_bundle_v1.tar"

#
# setup RPM (standard)
#
function package_setup_rpm {
  cdhere=$(pwd)
  echo "setup $RPM_RTE ..."
  restore -xvqf $RPM_RTE
  cd $cdhere
  mkdir /QOpenSys/opt
  cp -R usr/opt/* /QOpenSys/opt/.
  cp libcfg.a /QOpenSys/opt/freeware/lib/.
  cp libodm.a /QOpenSys/opt/freeware/lib/.
  rm -R usr
  ln -s /QOpenSys/opt /QOpenSys/var/opt
  ln -s /QOpenSys/opt /opt
  mkdir /var
  ln -s /QOpenSys/var/opt /var/opt
  ln -s /opt/freeware/bin/rpm /usr/bin/rpm
  cd /opt/freeware/lib
  ln -sf libdb-4.8.so libdb.so
  ln -sf libpopt.so.0 libpopt.so.0.0.0
  ln -sf libpopt.so.0 libpopt.so
  ln -sf librpm.so.2.0.3 librpm.so.0.0.0
  ln -sf librpm.so.2.0.3 librpm.so
  ln -sf librpmbuild.so.2.0.1 librpmbuild.so.0.0.0
  ln -sf librpmbuild.so.2.0.1 librpmbuild.so
  ln -sf librpmio.so.2.0.1 librpmio.so
  ln -sf librpmsign.so.0.0.1 librpmsign.so
  cd $cdhere
  rpm --version
  echo "setup $RPM_WGET ..."
  if [ -f /opt/freeware/bin/wget400 ]
  then
    wget400 --version
  else
    rpm --ignoreos --ignorearch --nodeps --replacepkgs -hUv $RPM_WGET
    cp /opt/freeware/bin/wget /opt/freeware/bin/wget400
    wget400 --version
  fi
}

function package_require_rpm {
  if (($system_OS400==1)); then
    # rpm available?
    if test -e /usr/bin/rpm; then
      return 1
    fi
    # setup rpm
    package_setup_rpm
    # rpm available?
    if test -e /usr/bin/rpm; then
      return 1
    fi
    echo "Error: /usr/bin/rpm not found"
    exit
  else
    return 1
  fi
}

#
# setup YUM (standard)
#
function package_setup_yum {
  tar -xf $YUM_TAR
  rpm --ignoreos --ignorearch --nodeps --ignoresize --replacepkgs --noscripts -hUv *.rpm

  # requires libcrypto.a (PASE)
  if test -e /opt/freeware/lib/libcrypto.a; then
    echo "found /opt/freeware/lib/libcrypto.a"
  else
    cp /usr/lib/libcrypto.a /opt/freeware/lib/libcrypto.a
  fi
  ar -x /opt/freeware/lib/libcrypto.a
  cp libcrypto.so.1.0.0 libcrypto.so
  ar -ruv /opt/freeware/lib/libcrypto.a libcrypto.so

  # requires libssl.a (PASE)
  if test -e /opt/freeware/lib/libssl.a; then
    echo "found /opt/freeware/lib/libssl.a"
  else
    cp /usr/lib/libssl.a /opt/freeware/lib/libssl.a
  fi
  ar -x /opt/freeware/lib/libssl.a
  cp libssl.so.1.0.0 libssl.so
  ar -ruv /opt/freeware/lib/libssl.a libssl.so

  # iconv hack between perzl and aix toolbox
  ar -x /usr/lib/libiconv.a
  ar -ruv /opt/freeware/lib/libiconv.a shr4.o

  # need yum dir
  mkdir -p /var/lib/rpm/Packages
  mkdir -p /var/log
  touch /var/log/yum.log

  # hack yum security non-root/non-QSECOFR 
  # (Aaron asked, blame him)
  cp yumcommands.py /opt/freeware/share/yum-cli/.
  cp yumupd.py /opt/freeware/share/yum-cli/.
}

function package_require_yum {
  if (($system_OS400==1)); then
    # yum available?
    if test -e /opt/freeware/bin/yum; then
      return 1
    fi
    # setup yum
    package_setup_yum
    # yum available?
    if test -e /opt/freeware/bin/yum; then
      return 1
    fi
    echo "Error: /opt/freeware/bin/yum not found"
    exit
  else
    return 1
  fi
}
#
# main
#
package_require_rpm
package_require_yum


